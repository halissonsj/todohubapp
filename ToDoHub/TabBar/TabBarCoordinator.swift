//
//  TabBarCoordinator.swift
//  ToDoHub
//
//  Created by Halisson da Silva Jesus on 06/03/20.
//  Copyright © 2020 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

class TabBarCoordinator: BaseCoordinator {
    
    typealias V = HomeViewController
    var navigationController: TDHNavigationController!
    var window: UIWindow
    
    //views
    var view: HomeViewController!
    private var homeCoordinator: HomeCoordinator
    
    var tabBar: TabBarController
    
    init(window: UIWindow) {
        self.window = window
        homeCoordinator = HomeCoordinator()
        
        tabBar = TabBarController()
    }
    
    func start() {
        tabBar.viewControllers = [homeCoordinator.start()]
        window.rootViewController = tabBar
    }
}
