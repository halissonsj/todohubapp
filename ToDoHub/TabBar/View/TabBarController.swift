//
//  TabBarController.swift
//  ToDoHub
//
//  Created by Halisson da Silva Jesus on 06/03/20.
//  Copyright © 2020 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

private enum NotificationNames: String {
    case tabBarDidSelect
}

extension Notification.Name {
    static let tabBarDidSelect = Notification.Name(NotificationNames.tabBarDidSelect.rawValue)
}

class TabBarController: UITabBarController {

    let controllerKey = "viewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    func setup() {
        delegate = self
    }
}

extension TabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        NotificationCenter.default.post(name: .tabBarDidSelect,
                                        object: nil,
                                        userInfo: [controllerKey: viewController])
        viewController.dismiss(animated: false, completion: nil)
        guard let navigation = (viewController as? UINavigationController) else { return }
        navigation.popToViewController(navigation.viewControllers[0], animated: false)
    }
}
