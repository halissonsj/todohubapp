//
//  AppCoordinator.swift
//  ToDoHub
//
//  Created by Halisson da Silva Jesus on 06/03/20.
//  Copyright © 2020 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

class AppCoordinator: BaseCoordinator {
    typealias V = TabBarController
    
    var window: UIWindow!
    var view: TabBarController!
    var navigationController: TDHNavigationController!
    
    var tabBarCoordinator: TabBarCoordinator?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        tabBarCoordinator = TabBarCoordinator(window: self.window)
//        tabBarCoordinator?.delegate = self
        tabBarCoordinator?.start()
    }
    
}
