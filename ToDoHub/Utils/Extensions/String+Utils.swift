//
//  String+Utils.swift
//  ToDoHub
//
//  Created by Halisson da Silva Jesus on 07/03/20.
//  Copyright © 2020 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

extension String {
    func remove(_ values: [String]) -> String {
        var stringValue = self
        let toRemove = stringValue.filter { values.contains("\($0)") }
        
        for value in toRemove {
            if let range = stringValue.range(of: "\(value)") {
                stringValue.removeSubrange(range)
            }
        }
        
        return stringValue
    }
}
