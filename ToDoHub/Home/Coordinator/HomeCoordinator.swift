//
//  HomeCoordinator.swift
//  ToDoHub
//
//  Created by Halisson da Silva Jesus on 06/03/20.
//  Copyright © 2020 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

class HomeCoordinator: BaseCoordinator {
    typealias V = HomeViewController
    var navigationController: TDHNavigationController!
    
    var view: HomeViewController!
    var viewModel: HomeViewModel!
    
    func start() -> TDHNavigationController {
        viewModel = HomeViewModel()
        view = HomeViewController(viewModel: viewModel)
        
        navigationController = TDHNavigationController(rootViewController: view)
        navigationController.tabBarItem.title = "Home"
        return navigationController
    }
}
