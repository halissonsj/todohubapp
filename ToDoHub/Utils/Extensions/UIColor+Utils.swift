//
//  UIColor+Utils.swift
//  ToDoHub
//
//  Created by Halisson da Silva Jesus on 07/03/20.
//  Copyright © 2020 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let tdhBackgroundColor = UIColor(hexString: "F9FCFF")!
    
    convenience init?(hexString: String) {
        let hexString = hexString.trimmingCharacters(in: .whitespacesAndNewlines).remove(["#"])
        guard let hexNumber = UInt32(hexString, radix: 16) else {
            return nil
        }
        self.init(hex6: hexNumber)
    }
    
    public convenience init(hex6: UInt32, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
        let green = CGFloat((hex6 & 0x00FF00) >> 8) / divisor
        let blue = CGFloat(hex6 & 0x0000FF) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
