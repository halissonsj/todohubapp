//
//  Coordinator.swift
//  ToDoHub
//
//  Created by Halisson da Silva Jesus on 06/03/20.
//  Copyright © 2020 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

protocol BaseCoordinator: AnyObject {
    
    associatedtype V: UIViewController
    var view: V! { get set }
    var navigationController: TDHNavigationController! { get set }
    
    func start() -> V
}

extension BaseCoordinator {
    func start() -> V {
        if view == nil {
            fatalError("You cannot start coordinator without initialize property view!")
        }
        return self.view!
    }
}
